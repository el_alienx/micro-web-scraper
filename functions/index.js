const functions = require('firebase-functions');
const cors = require('cors')({ origin: true });
const cheerio = require('cheerio');
const fetch = require('node-fetch');

const url = "https://www.theguardian.com/uk-news/"
let $ = cheerio;
let $container = cheerio;

const scrapeStories = async (text) => {
    const res = await fetch(text);
    const html = await res.text();
    const newscast = []
    const limit = 10

    $ = cheerio.load(html);
    $container = $('.fc-item__container').slice(0, limit)

    for (let index = 0; index < limit; index++) {
        newscast[index] = {
            id: index,
            category: myCategories()[index],
            imageURL: myImages()[index],
            media: false,
            title: myTitles()[index],
            url: myURLS()[index]
        }
    }

    return Promise.all(newscast);
}

const myTitles = () => {
    const results = []
    $('.fc-item__title .js-headline-text', $container).map((i, element) => {
        results[i] = $(element).text().trim();
    })
    return results
}

const myCategories = () => {
    const results = []
    $('.fc-item__title .fc-item__kicker', $container).map((i, element) => {
        results[i] = $(element).text()
    })
    return results
}
const myURLS = () => {
    const results = []
    $('.fc-item__title a', $container).map((i, element) => {
        results[i] = $(element).attr('href')
    })
    return results
}

const myImages = () => {
    const results = []
    $('img', $container).map((i, element) => {
        results[i] = $(element).attr('src')
    })
    return results
}


exports.scraper = functions.https.onRequest((request, response) => {
    cors(request, response, async () => {
        const data = await scrapeStories(url);

        response.send(data)
    });
});